import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderMessengerComponent } from './header-messenger.component';

describe('HeaderMessengerComponent', () => {
  let component: HeaderMessengerComponent;
  let fixture: ComponentFixture<HeaderMessengerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderMessengerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderMessengerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
