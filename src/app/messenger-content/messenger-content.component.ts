import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-messenger-content',
  templateUrl: './messenger-content.component.html',
  styleUrls: ['./messenger-content.component.scss']
})
export class MessengerContentComponent implements OnInit {

  messengerContent = messenger;
  constructor() { }

  ngOnInit(): void {
  }

}
export interface Messenger {
  cover: string;
  message: string
}
export const messenger = {
  cover: '',
  message: ''
};
