import { Users } from './../users';

import { Component, OnInit } from '@angular/core';

import { userName } from '../header-messenger/header-messenger.component';

import { messenger } from '../messenger-content/messenger-content.component';

import { inforMessenger } from '../infor-messenger/infor-messenger.component';

@Component({
  selector: 'app-user-list-chat',
  templateUrl: './user-list-chat.component.html',
  styleUrls: ['./user-list-chat.component.scss']
})
export class UserListChatComponent implements OnInit {

  users = Users;

  userName = userName;

  mess = messenger;

  infor = inforMessenger;

  constructor() {



  }

  ngOnInit(): void {
  }

  chooseItem(id: number, name: string, phone: string, message: string, cover: string) {

    userName.userName = name;

    messenger.message = message;
    messenger.cover = cover;

    inforMessenger.cover = cover;
    inforMessenger.name = name;
    inforMessenger.phone = phone;

  }



}
