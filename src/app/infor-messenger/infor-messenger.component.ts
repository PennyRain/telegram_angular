import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-infor-messenger',
  templateUrl: './infor-messenger.component.html',
  styleUrls: ['./infor-messenger.component.scss']
})
export class InforMessengerComponent implements OnInit {


  inforMessenger = inforMessenger;

  constructor() { }

  ngOnInit(): void {
  }

}

export interface InforMessenger {
  cover: string;
  name: string,
  phone: string
}
export const inforMessenger = {
  cover: '',
  name: '',
  phone: ''
};
