import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListChatComponent } from './list-chat/list-chat.component';
import { MessengerComponent } from './messenger/messenger.component';
import { InforMessengerComponent } from './infor-messenger/infor-messenger.component';
import { MyInfoComponent } from './my-info/my-info.component';
import { HeaderListChatComponent } from './header-list-chat/header-list-chat.component';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatIconModule} from '@angular/material/icon';
import { UserListChatComponent } from './user-list-chat/user-list-chat.component';
import { HeaderMessengerComponent } from './header-messenger/header-messenger.component'
import {MatCardModule} from '@angular/material/card';
import { BoxMessengerComponent } from './box-messenger/box-messenger.component';
import { InputMessengerComponent } from './input-messenger/input-messenger.component';
import { MessengerContentComponent } from './messenger-content/messenger-content.component';

@NgModule({
  declarations: [
    AppComponent,
    ListChatComponent,
    MessengerComponent,
    InforMessengerComponent,
    MyInfoComponent,
    HeaderListChatComponent,
    UserListChatComponent,
    HeaderMessengerComponent,
    BoxMessengerComponent,
    InputMessengerComponent,
    MessengerContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
