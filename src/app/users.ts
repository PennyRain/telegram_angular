export interface User {
    id: number;
    name: string;
    cover: string;
    phone: string;
    messenger: string;
}

export const Users = [
    {
        id : 1,
        name : 'NVA',
        cover : 'https://material.angular.io/assets/img/examples/shiba1.jpg',
        phone : '0966357583',
        messenger: 'Hello world'
    },
    {
        id : 2,
        name : 'NVB',
        cover : 'https://getwalls.io/media/large/2020/07/lion-4k-8k-hd-backgrounds-images-large-1068481658.jpg',
        phone : '0845991041',
        messenger: 'Hello Tuan'
    }
];
